# Empreintes

Exposition Empreintes - Brouage, espace Champlain. 04/2022 - 10/2022
Pièce sonore constituée d'un ensemble de poésies


https://centreintermondes.com/


Liste des auteurs :
- Estala López Solís : El hilo la cadena (Le fil est la chaîne)
- Estala López Solís et Patrick Beaulieu : L’étang perdu
- Isabelle Hubert : Empreintes
- Fares Chalabi : Dialog des Kulturs
- Jean d’Amérique : Suis-je pauvre ?
- Lolita Monga
- Monique Durand : Brouage, avoir été
- Sedjro Giovanni Houansou : Brouage
- Azad Ziya Eren : Souvenirs de mille ans pour Charles Baudelaire
- Laure Gouraige : Extrait exposition Espace Champlain
- Cyril Juvenil Assomo : Maculé
- Benjémy : Daddy


Partenariat : En collaboration avec le Département de la Charente Maritime et Collectif Essence Carbone : Jérôme Abel,
Mathieu Duvignaud, Vincent Ruffin, la participation de nombreux auteurs et autrices en résidences à Intermondes et Maison des Écritures pour le projet de Jérôme Abel et le soutien des Sauniers de l’île de Ré.

Dans leurs travaux, Jérôme Abel, Mathieu Duvignaud et Vincent Ruffin utilisent le multimédia, le land art, la peinture et le verre. Pour cette exposition à l'Espace Champlain, les artistes proposent de nous faire vivre une expérience en s’inspirant de l’histoire de Brouage pour nous conduire vers une vision actuelle de notre histoire contemporaine : surexploitation des ressources naturelles, migrations, déplacements de population, climat, xénophobie, mondialisation…
