# RPI SETUP - LIGHT
Sources : 
- https://medium.com/codex/speed-up-your-raspberry-pi-configuration-1de16f894de8
- https://www.makeuseof.com/tag/lightweight-operating-systems-raspberry-pi/
- https://rt-labs.com/docs/p-net/prepare_raspberrypi.html
- https://www.furkantokac.com/rpi3-fast-boot-less-than-2-seconds/
- https://github.com/IronOxidizer/instant-pi
- https://panther.software/configuration-code/raspberry-pi-3-4-faster-boot-time-in-few-easy-steps/

## SD CARD
- sd card 8Go, class 10, Samsung microSDHC cards
- install rpi os lite : 2022-04-04-raspios-bullseye-armhf-lite.img.xz
- balenaEtcher
- SSH : créer un fichier "ssh" dans boot de la sd card : touch <SDCARD>/boot/ssh (sinon raspi-config
- Wifi : nano <SDCARD>/boot/wpa_supplicant.conf :
'''
country=FR
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
	ssid="linksys"
	key_mgmt=NONE
	priority=2
}

network={
        ssid="AndroidAPR"
        psk="mypassword"
	priority=1
}
'''
- Network : sudo nano <SDCARD>/etc/network/interfaces
'''
auto lo
iface lo inet loopback
iface eth0 inet dhcp
# allow-hotplug eth0

allow-hotplug wlan0
auto wlan0
iface wlan0 inet manual
wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf
'''

## SETUP (via SSH)
- sudo apt update
- sudo apt upgrade -y
- sudo apt clean
- sudo apt autoremove
- sudo raspi-config : location, keyboard, ssh, wifi, hostname : rpi1 ou rpi2 ou rpi3 / rpi
- sudo nano /boot/config.txt : boot_delay=0
- sudo nano /etc/security/limits.conf
'''
@audio   -  rtprio     98
@audio   -  memlock    unlimited
@audio   -  nice      -19
'''


## BOOT
- nano /home/pi/start.sh
'''
#!/bin/bash

/usr/local/bin/pd -open /home/pi/empreintes/pd/main.pd -audiooutdev 3 -noadc -alsa -nomidi -rt -nogui -send "ID 1" -send "RPI 1" &
'''
- chmod a+x start.sh

- crontab -e
'''
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
@reboot sleep 30 && /home/pi/start.sh
'''

### Build Pure Data
(libjack-jackd2-dev)
- sudo apt-get install -y git build-essential automake autoconf libtool gettext libasound2-dev tcl tk libftgl2 
- git clone https://github.com/pure-data/pure-data
- cd pure-data
- ./autogen.sh
- ./configure
- make
- sudo make install

## CMD
- speaker-test -t wav -c 2
- sudo iwlist wlan0 scan
- wpa_cli -i wlan0 reconfigure
