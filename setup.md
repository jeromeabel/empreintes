
## RPI setup n°1
- sd card 16Go, class 10
- install rpi os full : 2022-04-04-raspios-bullseye-armhf-full.img.xz
- balenaEtcher
- rpi setup (location, keyboard layout, wifi, enable ssh, changer hostname+pass) : rpi1 ou rpi2 ou rpi3 / rpi
- update / upgrade


## sounds cmd
- Source : https://madskjeldgaard.dk/posts/sox-tutorial-split-by-silence/
- normalize, mono: for file in *.wav; do sox "$file" "../$file" norm -0.1 channels 1; done
- sox threebursts.wav burst_num.wav silence 1 0.1 1% 1 0.1 1% : newfile : restart
- sox -V3 audiobook.mp3 audiobook_part_.mp3 silence 1 0.5 0.1% 1 0.5 0.1% : newfile : restart


### BOOT with LXDE
mkdir ~/.config/autostart
sudo nano ~/.config/autostart/start.desktop
'''
[Desktop Entry]
Type=Application
Exec=bash /home/pi/start.sh
'''
sudo nano /home/pi/start.sh
'''
#!/bin/bash
sleep 30
pd -open /home/pi/empreintes/pd/main.pd -audiooutdev 3 -nogui -send "ID 3" -send "RPI 1" &
'''

chmod a+x start.sh


## pd
- sudo apt-get update
- sudo apt-get upgrade
- sudo apt-get install build-essential automake autoconf libtool gettext libasound2-dev tcl tk libftgl2 libjack-jackd2-dev
- git clone https://github.com/pure-data/pure-data
- ./autogen.sh
- ./configure --enable-jack
- make
- sudo make install


## clean
sudo apt purge libreoffice*
sudo apt purge wolfram-engine
sudo apt purge sonic-pi
sudo apt purge minecraft-pi
sudo apt clean
sudo apt autoremove



## fast boot
Run sudo raspi-config and disable 4th option: Wait for Network at Boot.

https://www.furkantokac.com/rpi3-fast-boot-less-than-2-seconds/
https://github.com/IronOxidizer/instant-pi
https://panther.software/configuration-code/raspberry-pi-3-4-faster-boot-time-in-few-easy-steps/


systemd-analyze blame
sudo systemctl disable raspapd.service (access point)
sudo systemctl disable bluetooth.service
sudo nano /boot/config.txt
dtoverlay=disable-bt (Disable bluetooth RPi 4)
boot_delay=0


sudo nano /etc/systemd/system/network-online.target.wants/networking.service
[Service]
TimeoutStartSec=1sec

